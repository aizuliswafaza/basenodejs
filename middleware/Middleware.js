class Middleware {
    static validate(req, res, next){
        if(!req.headers['user-auth']){
            res.status(401).json({
                status: 401,
                message: "Header user-auth is null!"
            })
            return res.end()
        }

        next()
    }
}

module.exports = Middleware