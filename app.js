var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

const BaseQueryFilter = require('./queryFilters/BaseQueryFilter')
var JQueryDatatableResponse = require('./response/JQueryDatatableResponse');

const Sequelize = require('sequelize');
const db = require('./models');
var BaseResponse = require('./response/BaseResponse');
const Middleware = require('./middleware/Middleware')
// const MasterUser = require('./models').master_user;
// const MasterDepartment = require('./models').master_department;
// const UserDepartment = require('./models').user_department;
// const MasterRole = require('./models/').master_role;
// MasterUser.belongsTo(MasterRole);
// MasterUser.belongsToMany(MasterDepartment, { through: UserDepartment })
// MasterDepartment.belongsToMany(MasterUser, { through: UserDepartment })

const initModels = require('./models/init-models').initModels
var model = initModels(db.sequelize);
const MasterUser = model.master_user;
const MasterDepartment = model.master_department;
const MasterSubDepartment = model.master_sub_department;
const UserDepartment = model.user_department;
const MasterRole = model.master_role;

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', Middleware.validate, usersRouter);

app.get('/user', async (req, res) => {
  var baseResponse = new BaseResponse();
  var filter = new BaseQueryFilter(req.query)
  var queryFilter = {}
  console.log(filter.length)
  console.log(filter.getStart)
  if(req.query.search_text){
    queryFilter[Sequelize.Op.or] = [
      {
        username: {[Sequelize.Op.like]: '%' + req.query.search_text + '%'}
      }
    ]
  }

  if(req.query.id){
    queryFilter["id"] = req.query.id
  }

  const users = await MasterUser.findAndCountAll({
    limit: filter.length,
    offset: filter.getStart,
    where: queryFilter,
    include: [
      {
        model: MasterRole
      },
      {
        model: MasterDepartment,
        through: {attributes: []},
        include: [
          {
            model: MasterSubDepartment
          }
        ]
      }
    ]
  }).then((result) => {
    var jQueryDatatableResponse = new JQueryDatatableResponse();
    jQueryDatatableResponse.setRecordsFiltered(result.rows.length)
    jQueryDatatableResponse.setRecordsTotal(result.count)
    jQueryDatatableResponse.setData(result.rows)
    baseResponse.data = jQueryDatatableResponse;
    return res.status(baseResponse.status).send(baseResponse);
  });
});

app.post('/user', async (req, res) => {
  const user = await MasterUser.create({
    username: req.body.username,
    password: req.body.password,
    masterRoleId: req.body.masterRoleId
  });

  res.json({
    'status': 'OK',
    'messages': '',
    'data': user
  });
});

app.get('/department', async (req, res) => {
  const departments = await MasterDepartment.findAll({
    include: [
      {
        model: MasterSubDepartment
      }
    ]
  });
  res.json({
    'status': 'OK',
    'messages': '',
    'data': departments
  });
});

app.post('/user-department/:id', async (req, res) => {
  const user = await MasterUser.findOne({
    where: {
      id: req.params.id
    }
  });

  res.json({
    'status': 'OK',
    'messages': '',
    'data': user
  });
});

app.put('/user', async (req, res) => {
  const user = await MasterUser.findOne({
    where: {
      id: req.body.id
    }
  });

  user.username = req.body.username
  user.password = req.body.password

  // await UserDepartment.create({masterUserId: user.id, masterDepartmentId: req.body.masterDepartmentId})

  await user.setMaster_departments(req.body.masterDepartmentIds)

  user.save()


  res.json({
    'status': 'OK',
    'messages': '',
    'data': user
  });
});

app.delete('/user/:id', async (req, res) => {
  const user = await MasterUser.destroy({
    where: {
      id: req.params.id
    }
  });

  res.json({
    'status': 'OK',
    'messages': '',
    'data': user
  });
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
app.listen(3000, () => console.log('system has been started on port 3000'))

let loop = true;

setTimeout(() => {  // callback execution is unreachable because main thread is still busy with below infinite loop code. 
    console.log('It will never reach here :ohh');
    loop = false;
}, 1000);

for (let index = 0; index < 10; index++) {
  setTimeout(() => {  // callback execution is unreachable because main thread is still busy with below infinite loop code. 
    console.log('It will never reach here :ohh');
    loop = false;
  }, 1000);
}

console.log('after loop');

module.exports = app;
