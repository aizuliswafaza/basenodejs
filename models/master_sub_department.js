const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('master_sub_department', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    masterDepartmentId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'master_department',
        key: 'id'
      }
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'master_sub_department',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "FK_master_sub_department_master_department",
        using: "BTREE",
        fields: [
          { name: "masterDepartmentId" },
        ]
      },
    ]
  });
};
