var DataTypes = require("sequelize").DataTypes;
var _master_department = require("./master_department");
var _master_role = require("./master_role");
var _master_sub_department = require("./master_sub_department");
var _master_user = require("./master_user");
var _user_department = require("./user_department");

function initModels(sequelize) {
  var master_department = _master_department(sequelize, DataTypes);
  var master_role = _master_role(sequelize, DataTypes);
  var master_sub_department = _master_sub_department(sequelize, DataTypes);
  var master_user = _master_user(sequelize, DataTypes);
  var user_department = _user_department(sequelize, DataTypes);

  master_department.belongsToMany(master_user, { through: user_department, foreignKey: "masterDepartmentId", otherKey: "masterUserId" });
  master_user.belongsToMany(master_department, { through: user_department, foreignKey: "masterUserId", otherKey: "masterDepartmentId" });
  master_sub_department.belongsTo(master_department, { foreignKey: "masterDepartmentId"});
  master_department.hasMany(master_sub_department, { foreignKey: "masterDepartmentId"});
  master_user.belongsTo(master_role, { foreignKey: "masterRoleId"});
  master_role.hasMany(master_user, { foreignKey: "masterRoleId"});
  user_department.belongsTo(master_user, { foreignKey: "masterUserId"});
  master_user.hasMany(user_department, { foreignKey: "masterUserId"});
  user_department.belongsTo(master_department, { foreignKey: "masterDepartmentId"});
  master_department.hasMany(user_department, { foreignKey: "masterDepartmentId"});

  return {
    master_department,
    master_role,
    master_sub_department,
    master_user,
    user_department,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
