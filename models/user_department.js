const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user_department', {
    masterUserId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'master_user',
        key: 'id'
      }
    },
    masterDepartmentId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'master_department',
        key: 'id'
      }
    },
    entry_date: {
      type: DataTypes.DATE,
      allowNull: true
    },
    out_date: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'user_department',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "masterUserId" },
          { name: "masterDepartmentId" },
        ]
      },
      {
        name: "FK_master_department_user_department",
        using: "BTREE",
        fields: [
          { name: "masterDepartmentId" },
        ]
      },
      {
        name: "FK_master_user_user_department",
        using: "BTREE",
        fields: [
          { name: "masterUserId" },
        ]
      },
    ]
  });
};
