const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('master_user', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    username: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    password: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    masterRoleId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'master_role',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'master_user',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "FK_master_role_master_user",
        using: "BTREE",
        fields: [
          { name: "masterRoleId" },
        ]
      },
    ]
  });
};
