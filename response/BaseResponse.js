class BaseResponse{
    
    constructor(body){
        this.status =  200;
        this.message = '';
        this.data = null;
    }
}

module.exports = BaseResponse;