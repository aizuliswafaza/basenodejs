function JQueryDatatableResponse(){
    this.draw = 1;
    this.records_total = 0;
    this.records_filtered = 0;
    this.data = []
}

JQueryDatatableResponse.prototype.setDraw = function(draw){
    this.draw = draw;
}

JQueryDatatableResponse.prototype.getDraw = function(){
    return this.draw;
}

JQueryDatatableResponse.prototype.setRecordsTotal = function(records_total){
    this.records_total = records_total;
}

JQueryDatatableResponse.prototype.getRecordsTotal = function(){
    return this.records_total;
}

JQueryDatatableResponse.prototype.setRecordsFiltered = function(records_filtered){
    this.records_filtered = records_filtered;
}

JQueryDatatableResponse.prototype.getRecordsFiltered = function(){
    return this.records_filtered;
}

JQueryDatatableResponse.prototype.setData = function(data){
    this.data = data;
}

JQueryDatatableResponse.prototype.getData = function(){
    return this.data;
}

module.exports = JQueryDatatableResponse;