class BaseQueryFilter{
    
    constructor(body){
        this.draw =  body.draw == undefined ? 1: body.draw;
        this.page = body.page == undefined ? 0: body.page;
        this.length = body.length == undefined ? 10: Number(body.length);
        this.sort = body.sort == undefined ? []: body.sort;
        this.search_text = body.search_text == undefined ? null: body.search_text;
        this.id = body.id == undefined ? null: body.id;
    }

    get getStart(){
        if(this.page > 0 && this.length > 0){
            return (this.page - 1) * this.length;
        }

        return 0;
    }
}

module.exports = BaseQueryFilter;